﻿var miruSite = angular.module('MiruSite',[]);

miruSite.controller('Posts', ['$scope','$timeout','$http','postService', function($scope, $timeout, $http, postService) {
	postService.getPosts()
		.then(function(result) {
			$scope.posts = result[0].data.posts.slice(0,100);
			$timeout(function() {
				$("#youtube_comment").removeClass("thumb").addClass("thumb1");
				
				h5u_parallelism.init({
					introDelay: 0,
				});
				
				$("#reel").css("visibility","visible");
				$(".inner").css("opacity","1");
			}, 200);
		});
}]);


miruSite.factory('postService', function($http, $q) {
	return {
		getPosts: function() {
			return $q.all([
				$http.get('http://gplusparser-48.appspot.com/memberPost/103948509378248185139')
			]);
		}
	};
});